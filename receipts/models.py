from django.db import models
from django.conf import settings


class ExpenseCategory(models.Model):
    # name property characters (max_length=50)
    name = models.CharField(max_length=50)

    # owner property, foreign key to the User model with
    # related name categories
    # cascade deletion relation
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    # name property that contains characters with max length 100 char
    name = models.CharField(max_length=100)

    # number property contains char(not numbers) with max length of 20 char
    number = models.CharField(max_length=20)

    # owner property, foreign key to the User model with
    # ralated name accounts
    # cascase deletion relation
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    # primary thing that this app keeps track for accounting purposes
    # introduces the DecimalField, field always want to store money values

    # vendor property contains characters with max length 200 char
    vendor = models.CharField(max_length=200)

    # total property that is a DecimalField
    # three decimal places
    # max 10 digits
    total = models.DecimalField(max_digits=10, decimal_places=3)

    # tax property that is a DecimalField
    # three decimal places
    # max 10 digits
    tax = models.DecimalField(max_digits=10, decimal_places=3)

    # date property contains a date and time of when transaction took place
    date = models.DateTimeField()

    # purchaser property that is a foreignkey to the User model
    # related name of "receipts"
    # cascase deletion relation
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    # category property that is a foreign key to the ExpenseCategory model
    # related name of "receipts"
    # cascase deletion relation
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    # account property that is a foreign key to the Account model
    # related name of "receipts"
    # cascase deletion relation
    # allowed to be null
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
